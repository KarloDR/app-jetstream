<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
$chart2 = (new ArielMejiaDev\LarapexCharts\LarapexChart())->areaChart()
        ->setTitle('Users')
        ->addArea('Active users', [10, 30, 25])
        ->addArea('Inactive users', [5, 15, 35])
        ->setColors(['#ffc63b', '#ff6384'])
        ->toVue();

        $chart = (new ArielMejiaDev\LarapexCharts\LarapexChart())->barChart()
            ->setTitle('San Francisco vs Boston.')
            ->setSubtitle('Wins during season 2021.')
            ->addData('San Francisco', [6, 9, 3, 4, 10, 8])
            ->addData('Boston', [7, 3, 8, 2, 6, 4])
            ->setXAxis(['January', 'February', 'March', 'April', 'May', 'June'])
            ->toVue();

        $chart3 = (new ArielMejiaDev\LarapexCharts\LarapexChart())->polarAreaChart()
            ->setTitle('Top 3 scorers of the team.')
            ->setSubtitle('Season 2021.')
            ->addData([20, 24, 30])
            ->setLabels(['Player 7', 'Player 10', 'Player 9'])
            ->toVue();

//     return Inertia::render('Dashboard');
    return Inertia::render('Dashboard', compact('chart', 'chart2', 'chart3'));
})->name('dashboard');

Route::middleware(['auth:sanctum', 'verified'])->get('/meeting', function () {
    $chart = (new ArielMejiaDev\LarapexCharts\LarapexChart())->barChart()
                ->setTitle('San Francisco vs Boston.')
                ->setSubtitle('Wins during season 2021.')
                ->addData('San Francisco', [6, 9, 3, 4, 10, 8])
                ->addData('Boston', [7, 3, 8, 2, 6, 4])
                ->setXAxis(['January', 'February', 'March', 'April', 'May', 'June'])
                ->toVue();

    return Inertia::render('MeetingRatings', compact('chart'));
})->name('meetingRatings');

Route::middleware(['auth:sanctum', 'verified'])->get('/rocks', function () {
    return Inertia::render('Rocks');
})->name('rocks');

Route::middleware(['auth:sanctum', 'verified'])->get('/agenda', function () {
    return Inertia::render('Agenda');
})->name('agenda');

Route::middleware(['auth:sanctum', 'verified'])->get('/previousRocks', function () {
    return Inertia::render('PreviousRocks');
})->name('previousRocks');

Route::get('/chart', function() {

    // Simple example
    $chart = (new ArielMejiaDev\LarapexCharts\LarapexChart())->areaChart()
        ->setTitle('Users')
        ->addArea('Active users', [10, 30, 25])
        ->addArea('Inactive users', [5, 15, 35])
        ->setColors(['#ffc63b', '#ff6384'])
        ->toVue();


    return Inertia::render('Chart', compact('chart'));

});
